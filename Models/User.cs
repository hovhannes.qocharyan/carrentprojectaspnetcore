﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarRent.Models
{
    public class User : IdentityUser
    {
        
        public string PassportId { get; private set; }
        public string DriverLicense { get; private set; }
        public string FirstName { get; private set; }
        public string MiddleName { get; private set; }
        public string LastName { get; private set; }
        public DateTime BirthDay { get; private set; }
        public decimal Balance { get; private set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }


        // IdnetityUser have prop Email(LoginName).
        // IdentityUser have prop Password. 

        public User(string passportId, string driverLicense,string email, string firstName, string middleName, string lastName, DateTime birthDay, string phone1, string phone2)
        {
            this.PassportId = passportId;
            this.DriverLicense = driverLicense;
            this.Email = email;
            this.UserName = email;
            this.FirstName = firstName;
            this.MiddleName = middleName;
            this.LastName = lastName;
            this.BirthDay = birthDay;
            this.Phone1 = phone1;
            this.Phone2 = phone2;
            
            
        }




    }
}
