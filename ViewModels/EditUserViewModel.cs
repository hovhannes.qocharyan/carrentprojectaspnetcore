﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarRent.ViewModels
{
    public class EditUserViewModel
    {
        public string Id { get; set; }
        
        public string PassportId { get; set; }

        public string DriverLicense { get; set; }
       
        public string FirstName { get; set; }
        
        public string MiddleName { get; set; }
       
        public string LastName { get; set; }
        
        public DateTime BirthDay { get; set; }
        
        public string Phone1 { get; set; }
       

        public string Phone2 { get; set; }
       
        public string Email { get; set; }
       
        
    }
}
