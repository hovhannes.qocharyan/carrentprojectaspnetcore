﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CarRent.ViewModels
{
    public class RegisterViewModel
    {
       
       [Required(ErrorMessage ="Passport Id is required")]
       [Display(Name ="Passport Id")]
       public string PassportId { get; set; }

        [Required(ErrorMessage ="Driver license is required")]
        [Display(Name ="Driver license")]
        public string DriverLicense { get; set; }
        [Required(ErrorMessage ="Please enter your first name")]
        [Display(Name ="First Name")]
        [DataType(DataType.Text)]

        public string FirstName { get; set; }
        [Required(ErrorMessage ="Please enter your middle name")]
        [Display(Name ="Middle Name")]
        [DataType(DataType.Text)]

        public string MiddleName { get; set; }
        [Required(ErrorMessage ="Please enter your last name")]
        [Display(Name ="Last Name")]
        [DataType(DataType.Text)]
        public string LastName { get; set; }
        [Required(ErrorMessage ="Please enter your birthday")]
        [Display(Name ="Birthday")]
        [DataType(DataType.Date)]
        public DateTime BirthDay{ get; set; }
        [Display(Name ="Phone1")]
        [DataType(DataType.PhoneNumber)]
        public string Phone1 { get; set; }
        [Display(Name ="Phone2")]
        [DataType(DataType.PhoneNumber)]

        public string Phone2 { get; set; }
        [Required(ErrorMessage ="Enter your email address")]
        [Display(Name ="Email(User Name)")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required(ErrorMessage ="Password is required")]
        [DataType(DataType.Password)]
        [Display(Name ="Password")]
        public string Password { get; set; }
        [Required(ErrorMessage ="PasswordConfirm is required")]
        [DataType(DataType.Password)]
        [Compare("Password" , ErrorMessage ="Password is incorrect")]
        [Display(Name ="Confirm Password")]
        public string PasswordConfirm { get; set; }
        

    }
}
