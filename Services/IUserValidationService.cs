﻿using CarRent.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarRent.Services
{
    public interface IUserValidationService
    {
        public Dictionary<string, string> UserValidation(User user);
    }
}
