﻿using CarRent.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CarRent.Services
{
    public class UserValidationService : IUserValidationService
    {
        private Dictionary<string, string> validationErrors;
        bool flag = true;
        public UserValidationService()
        {
            validationErrors = new Dictionary<string, string>();
        }
        public Dictionary<string, string> UserValidation(User user)
        {
            if (!Regex.IsMatch(user.PassportId, "[A-Z,a-z]{2}[0-9]{7}"))
            {
                flag = false;
                validationErrors.Add("Passport Id", "Passpord id is incorect please enter a valid passport id");
            }

            if(!Regex.IsMatch(user.DriverLicense , "[A-Z,a-z]{2}[0-9]{6}"))
            {
                flag = false;
                validationErrors.Add("DriverLicense", "Driver license is incorect please enter a valid driver license ");
            }

            if(!Regex.IsMatch(user.FirstName , "^[A-Z,a-z]{3,15}"))
            {
                flag = false;
                validationErrors.Add("FirstName", "Incorect first name, please enter new one");
            }
            if (!Regex.IsMatch(user.MiddleName, "^[A-Z,a-z]{3,15}"))
            {
                flag = false;
                validationErrors.Add("MiddleName", "Incorect middle name, please enter new one");
            }
            if (!Regex.IsMatch(user.LastName, "^[A-Z,a-z]{3,15}"))
            {
                flag = false;
                validationErrors.Add("LastName", "Incorect last name, please enter new one");
            }
            if (!ValidateDateTime(user.BirthDay))
            {
                validationErrors.Add("BirthDay", "Your age doesn't match");
            }

            if (flag)
            {
                return null;
            }
            else
            {
                return validationErrors;
            }

            

        }

        private bool ValidateDateTime(DateTime birthDay)
        {
            try
            {
                DateTime zeroTime = new DateTime(1, 1, 1);
                DateTime now = DateTime.Now;
                TimeSpan timeSpanDate = now - birthDay;
                int.TryParse(((zeroTime + timeSpanDate).Year - 1).ToString(), out int years);
                if (years < 18 || years > 80)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch
            {
                validationErrors.Add("BirthDay", "Inccorect date time ,please enter valid date time");
                return false;
            }
            
        }
    }
}
