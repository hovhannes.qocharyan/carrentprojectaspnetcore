﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CarRent.Models;
using CarRent.Services;
using CarRent.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace CarRent.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<User> userManager;
        private readonly SignInManager<User> signInManager;
        private readonly RoleManager<IdentityRole> roleManager;
        private   readonly IUserValidationService userValidation;
       
        public AccountController(UserManager<User> um, SignInManager<User> sm ,RoleManager<IdentityRole> rm , IUserValidationService uv)
        {
            this.userManager = um;
            this.signInManager = sm;
            this.roleManager = rm;
            this.userValidation = uv;

           
        }
        [HttpGet]
        public IActionResult Login(string returnUrl=null)
        {
            return View(new LoginViewModel { ReturnUrl = returnUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, false);
                if (result.Succeeded)
                {
                    if (!string.IsNullOrEmpty(model.ReturnUrl) && Url.IsLocalUrl(model.ReturnUrl))
                    {
                        return Redirect(model.ReturnUrl);
                    }
                    else
                    {
                        User signedUser = await userManager.FindByEmailAsync(model.Email);
                        IList<string> roles = userManager.GetRolesAsync(signedUser).Result;
                        if (roles.Contains("Administrator"))
                        {
                            return RedirectToAction("AdministratorPage", "Account");
                        }
                        else if (roles.Contains("Employee"))
                        {
                            return RedirectToAction("EmployeePage", "Account");
                        }
                        else
                        {
                            return RedirectToAction("UserPage", "Account");
                        }
                        

                       
                       
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Incorrect username or password");
                }
            }
            return View(model);
        }
        

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]

        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                User user = new User(model.PassportId, model.DriverLicense,model.Email, model.FirstName, model.MiddleName, model.LastName, model.BirthDay, model.Phone1, model.Phone2);
                Dictionary<string,string> validationResultErrors  = userValidation.UserValidation(user);


                if (validationResultErrors == null)
                {
                    var result = await userManager.CreateAsync(user, model.Password);

                    if (result.Succeeded)
                    {
                        await signInManager.SignInAsync(user, false);
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                       
                        foreach (var err in result.Errors)
                        {
                            ModelState.AddModelError("", err.Description);
                        }
                    }
                }
                else
                {
                    foreach (var err in validationResultErrors)
                    {
                        ModelState.AddModelError(err.Key, err.Value);
                    }
                }
                    
                
                

                
            }

                
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            await signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }

        public IActionResult AdministratorPage()
        {
            return View();
        }
        public IActionResult EmployeePage()
        {
            return View();
        }
        public IActionResult UserPage()
        {
            return View();
        }
        

    }
}