﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CarRent.Models;
using CarRent.Services;
using CarRent.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace CarRent.Controllers
{
    public class UsersController : Controller
    {
        private readonly UserManager<User> userManager;
        private readonly IUserValidationService userValidator;
        public UsersController(UserManager<User> um , IUserValidationService uv)
        {
            this.userManager = um;
            this.userValidator = uv;
        }

        public IActionResult Index() => View(userManager.Users.ToList());

        public IActionResult Create() => View();

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                User user = new User(model.PassportId, model.DriverLicense, model.Email, model.FirstName, model.MiddleName, model.LastName, model.BirthDay, model.Phone1, model.Phone2);
                Dictionary<string,string>userValidationErrors =  userValidator.UserValidation(user);
                if (userValidationErrors == null)
                {
                    var result = await userManager.CreateAsync(user, model.Password);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        foreach (var err in result.Errors)
                        {
                            ModelState.AddModelError("", err.Description);
                        }
                    }
                }
                else
                {
                    foreach (var err in userValidationErrors)
                    {
                        ModelState.AddModelError(err.Key, err.Value);
                    }
                }


            }
            return View(model);
        } 

        public async Task<IActionResult> Edit(string id)
        {
            User user =await userManager.FindByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            EditUserViewModel model = new EditUserViewModel
            {
                Id = user.Id,
                PassportId = user.PassportId,
                DriverLicense = user.DriverLicense,
                Email = user.Email,
                FirstName = user.FirstName,
                MiddleName = user.MiddleName,
                LastName = user.LastName,
                Phone1 = user.Phone1,
                Phone2 = user.Phone2,
                
            };

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(EditUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                User user = await userManager.FindByIdAsync(model.Id);
                if (user != null)
                {
                    user = new User(model.PassportId,model.DriverLicense,model.Email,model.FirstName,model.MiddleName,model.LastName,model.BirthDay, model.Phone1,model.Phone2);
                    Dictionary<string, string> userValidationErrors = userValidator.UserValidation(user);
                    if (userValidationErrors == null)
                    {
                        var result = await userManager.UpdateAsync(user);
                        if (result.Succeeded)
                        {
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            foreach (var err in result.Errors)
                            {
                                ModelState.AddModelError("", err.Description);
                            }
                        }
                    }
                    else
                    {
                        foreach (var err in userValidationErrors)
                        {
                            ModelState.AddModelError(err.Key, err.Value);
                        }
                    }
                  
                }
            }
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(string id)
        {
            User user = await userManager.FindByIdAsync(id);
            if (user != null)
            {
                IdentityResult result = await userManager.DeleteAsync(user);
            }
            return RedirectToAction("Index");
        }
    }
}